var express = require('express');
var router = express.Router();
var fs = require('fs')

var users = ""

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});


fs.readFile("././usersInfo/usersInfo.json", "utf8", (err, data) => {
  users = data
})
router.get('/get', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-type,Accept');
  res.send(users)
})

router.post('/post', (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-type,Accept');
  var usersJSON = req.body
  fs.writeFile('././usersInfo/usersInfo.json', JSON.stringify(usersJSON), "utf8", function (err) { if (err) { console.log(err); } })
  res.send("Sign up successull!")
})
module.exports = router;
