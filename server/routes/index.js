var express = require('express');
var router = express.Router();
const fs = require('fs')


var accessories = ""
var bags = ""
var clothes = ""
var shoes = ""


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});


fs.readFile("././shopItems/accessories.json", "utf8", (err, data) => {
  accessories = data
})
fs.readFile("././shopItems/bags.json", "utf8", (err, data) => {
  bags = data
})
fs.readFile("././shopItems/clothes.json", "utf8", (err, data) => {
  clothes = data
})
fs.readFile("././shopItems/shoes.json", "utf8", (err, data) => {
  shoes = data
})


router.get('/api/accessories', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-type,Accept');
  res.send(accessories)
})
router.get('/api/bags', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-type,Accept');
  res.send(bags)
})
router.get('/api/clothes', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-type,Accept');
  res.send(clothes)
})
router.get('/api/shoes', function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With,Content-type,Accept');
  res.send(shoes)
})


module.exports = router;
