import { clothes } from './itemsByJSON/clothes.json'
import { shoes } from './itemsByJSON/shoes.json'
import { bags } from './itemsByJSON/bags.json'
import { accessories } from './itemsByJSON/accessories.json'
import { store } from './../../App.js'
import axios from 'axios'
import React from 'react'


export const items = [...clothes, ...shoes, ...bags, ...accessories]
const sales = [];
const news = [];
var searchItems = [items[0], items[1]]
function searchEnter() {
    if (document.querySelector(".header__input") !== null) {
        const searchValue = document.querySelector(".header__input").value;
        searchItems = []
        for (var i = 0; i < items.length; i++) {
            if (items[i].name.includes(searchValue) || items[i].type.includes(searchValue))
                searchItems.push(items[i])
        }
    }
}
for (var i = 0; i < items.length; i++) {
    if ("sale" in items[i])
        sales.push(items[i])
    if ("new" in items[i])
        news.push(items[i])
}
function Item(props) {
    return (
        <div className="card item" id={props.name + "-" + props.id}>
            <img src={"Items/" + props.id + props.name + props.type + (".png")} className="card-img-top crop" alt="..." />
            <div className="card-body">
                <h5 className="card-title">{props.name}{props.new}</h5>
                <p className="card-text">Cost : {props.cost}$ {props.sale}</p>
            </div>
            <div className="card-body-2">
                <a href={"/item/" + props.id} className="card-link opachover cart">View</a>
                <div className="card-link opachover cart" onClick={() => {
                    if (store.getState().currentUser !== undefined) {
                        var usersInfo = props.usersInfo
                        usersInfo[store.getState().currentUser.userId].itemsList.push({
                            name: props.name,
                            id: props.id,
                            type: props.type,
                            cost: props.cost,
                            sale: props.sale,
                            new: props.new,
                            key: i
                        })
                        axios.post("/users/post", usersInfo)
                        store.getState().updateHeader();
                    }
                }}>Add +</div>
            </div>
        </div>
    )
}
export function Items(props) {
    // props : id name
    var items = props.items
    var handleItems = []
    for (var i = 0; i < items.length; i++) {
        handleItems.push(<Item
            name={items[i].name}
            id={items[i].id}
            type={items[i].type}
            cost={items[i].cost}
            sale={items[i].sale}
            new={items[i].new}
            key={i}
            usersInfo={props.usersInfo}
        />)
    }
    var RenderItems = handleItems
    return (
        <div className="itemCarry body">
            {RenderItems}
        </div>
    )
}

export function AllItem(props) {
    return (<Items items={items} usersInfo={props.usersInfo} />)
}
function New(props) {
    return (<Items items={news} usersInfo={props.usersInfo} />)
}
function Sale(props) {
    return (<Items items={sales} usersInfo={props.usersInfo} />)
}
function Clothes(props) {
    return (<Items items={clothes} usersInfo={props.usersInfo} />)
}
function Shoes(props) {
    return (<Items items={shoes} usersInfo={props.usersInfo} />)
}
function Bags(props) {
    return (<Items items={bags} usersInfo={props.usersInfo} />)
}
function Accessories(props) {
    return (<Items items={accessories} usersInfo={props.usersInfo} />)
}
function SearchItems(props) {
    // const[state,setState]=useState(0)
    // function update() {setState(state+1)}
    // store.dispatch({type:"update",func(states){
    //     states.updateSearch=update;
    //     return states
    // }})
    searchEnter();
    return (<Items items={searchItems} usersInfo={props.usersInfo} />)
}
export { Clothes }
export { Shoes }
export { Bags }
export { Accessories }
export { Sale }
export { New }
export default SearchItems