import "./../../css/Body/body.css"
import { BrowserRouter as Route, Switch, Redirect } from 'react-router-dom'
import SearchItems, { Clothes, Shoes, Bags, Accessories, Sale, New, items, AllItem } from './route.js'
import React, { useState } from "react"


function About() {
    return (
        <div className="about">
            Testing Shop <br />
            <span style={{ color: "red" }}>&#9888;&nbsp;Database is now ready, users infomation is now savable</span> <br />
            Setting button is on design processing <br />
            Enjoy! :{"))"} <br />
        </div>
    )
}
var Search = () => {
    return <></>
}
var SearchRoute = () => <></>
React.memo(SearchRoute)
export var searchItems = []
export default function Body(props) {
    const [state, setState] = useState(0)
    document.addEventListener("keydown", (e) => {
        if (e.key === "Enter" && document.querySelector(".header__input") === document.activeElement) {
            const searchValue = document.querySelector(".header__input").value;
            searchItems = []
            for (var i = 0; i < items.length; i++) {
                if (items[i].name.includes(searchValue) || items[i].type.includes(searchValue))
                    searchItems.push(items[i])
            }
            // document.querySelector(".itemSearch").style.display="block"
            // document.querySelector(".itemClick").style.display="none"
            Search = () => <Redirect to={"/search?=" + searchValue} />
            SearchRoute = () => <Route path={"/search?=" + searchValue}><SearchItems /></Route>
            setState(state + 1)
        }
    })
    document.addEventListener("click", (e) => {
        if (e.target.classList.contains("body") || e.target.classList.contains("header__nav-route"))
            if (window.location.href.includes("/search?")) {
                // document.querySelector(".itemSearch").style.display="none"
                // document.querySelector(".itemClick").style.display="block"
                searchItems = [];
                Search = () => <Redirect to="/all" />
                SearchRoute = () => <></>
                setState(state + 1)
            }
    })
    // document.addEventListener("keydown",(e)=>{
    //     if(e.key==="Enter")
    //     {
    //         Search=()=> <Redirect to={"/search/"+document.querySelector(".header__input").value}/>
    //         setState(state+1)
    //     }
    // })
    return (
        <div className="body bodyPart">
            <div className="AllItemCarry body itemClick" >
                <Switch>
                    <Route exact path='/'><AllItem usersInfo={props.usersInfo} /></Route>
                    <Route path='/all'><AllItem usersInfo={props.usersInfo} /></Route>
                    <Route path='/sale'><Sale usersInfo={props.usersInfo} /></Route>
                    <Route path='/newcollection'><New usersInfo={props.usersInfo} /></Route>
                    <Route path='/clothes'><Clothes usersInfo={props.usersInfo} /></Route>
                    <Route path='/shoes'><Shoes usersInfo={props.usersInfo} /></Route>
                    <Route path='/bags'><Bags usersInfo={props.usersInfo} /></Route>
                    <Route path='/accessories'><Accessories usersInfo={props.usersInfo} /></Route>
                    <Route path='/about'><About usersInfo={props.usersInfo} /></Route>
                    <SearchRoute usersInfo={props.usersInfo} />
                    <Route path='/:somestring'><h1 style={{ marginTop: "100px", fontSize: "65px", color: "red" }}>Error 404</h1></Route>
                </Switch>
            </div>
            <Search />
            {/* <div className="AllItemCarry body itemSearch" style={{display:"none"}}>
                
            </div> */}
        </div>

    )
}