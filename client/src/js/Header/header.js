import "./../../css/Header/header.css"
import "./../../css/Header/headerVersion2.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faEllipsisV, faShoppingCart, faSearch, faUserCircle, faUserPlus, faQuestionCircle, faUser } from '@fortawesome/free-solid-svg-icons'
import { useRef } from 'react'
import { Link } from 'react-router-dom'
import { SignIn, SignUp } from './Route/signinup.js'
import { store } from './../../App.js'
import { useState } from 'react';
import { CartItems, renewList } from './Route/cart.js'

var user = undefined;
export default function Header(props) {
    const [state, setState] = useState(0);
    function update() { setState(state + 1) }
    store.dispatch({
        type: "update", func: function (states) {
            states.updateHeader = update;
            return states;
        }
    })
    if (store.getState() !== undefined)
        user = store.getState().currentUser;
    const inputField = useRef();
    const inputCarry = useRef();
    function onSearch() {
        inputField.current.focus()
        inputField.current.value = ''
    }
    function offSearch() {
        inputField.current.value = ''
        inputField.current.blur();
    }
    function openBars() {
        var bars = document.querySelector(".boardCarry");
        if (bars != null) {
            bars.style.animation = `navbars cubic-bezier(.62,.25,.4,1.21) 0.5s forwards`
        }
        document.querySelector(".header-dashboard").style.animation = `navbarsresize cubic-bezier(.62,.25,.4,1.21) 0.5s forwards`
        // document.querySelector(".header__nav-route").style.animation = `routeresizebybar cubic-bezier(.62,.25,.4,1.21) 0.5s forwards`
        document.querySelector(".AllItemCarry").style.width = `calc(100vw - 200px)`;
        // setTimeout(() => {
        //     document.querySelector(".header__nav-route").style.position = `relative`;
        //     document.querySelector(".header__nav-route").style.transform = `translateX(-140px)`;
        // }, 500);
    }
    function openSignUp() {
        const signUp = document.querySelector(".signUpCarry");
        const disable = document.querySelector(".disableAll");
        signUp.style.display = "flex";
        signUp.style.animation = "show 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
        disable.style.display = "block";
        disable.style.animation = "show 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
    }
    function openSignIn() {
        const signIn = document.querySelector(".signInCarry");
        const disable = document.querySelector(".disableAll");
        signIn.style.display = "flex";
        signIn.style.animation = "show 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
        disable.style.display = "block";
        disable.style.animation = "show 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
    }
    function logOut() {
        document.querySelector(".userTabCarry").classList.toggle("tabShow")
        store.dispatch({
            type: "logOut", func: function (states) {
                states = {}
                return states
            }
        })
        renewList();
        update();
        document.getElementById("signIn").classList.remove("disabled")
        document.getElementById("currentUser").classList.add("disabled")
    }
    document.addEventListener("click", (e) => {
        if (!e.target.classList.contains("user"))
            document.querySelector(".userTabCarry").classList.remove("tabShow")
        if (!e.target.classList.contains("cart"))
            document.querySelector(".cartTabCarry").classList.remove("tabShow")
    })
    return (
        <>
            <div className="header__bar">
                <nav className="navbar navbar-expand-lg navbar-light header__nav">
                    <ul className="navbar-nav nav-dashboard_search">
                        <li className="nav-item header-dashboard" onClick={openBars}>
                            <div className="nav-link"><FontAwesomeIcon icon={faBars} /></div>
                        </li>
                        <li className="nav-item header-search" ref={inputCarry} >
                            <div className="nav-link">
                                <input type="abc" className="header__input btn" ref={inputField} onMouseOver={onSearch} onMouseLeave={offSearch}
                                    placeholder="type here"
                                ></input>
                            </div>
                            <div className="nav-link">
                                <div className="header__search-btn"  >
                                    <FontAwesomeIcon icon={faSearch} />
                                </div>
                            </div>
                        </li>
                        <li className="nav-item header-logo">
                            <Link to="/"><img src="katashop.png" alt="logo" className="nav-link header__logo" /></Link>
                        </li>
                    </ul>
                    <ul className="navbar-nav header__nav-route">
                        <li className="nav-item">
                            <Link to="/about" className="nav-link header__item-route"><FontAwesomeIcon icon={faQuestionCircle} />&nbsp; About</Link>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link header__item-route" onClick={openSignUp}><FontAwesomeIcon icon={faUserPlus} />&nbsp; Sign up</div>
                        </li>
                        <li className="nav-item">
                            <div className="nav-link header__item-route" id="signIn" onClick={openSignIn}><FontAwesomeIcon icon={faUser} />&nbsp; Sign in</div>
                        </li>
                    </ul>
                    <ul className="navbar-nav header__nav-user">
                        <li className="nav-item header-user user">
                            <div className="nav-link disabled header-user-icon user" onClick={() => {
                                document.querySelector(".userTabCarry").classList.toggle("tabShow")
                            }} id="currentUser"><span style={{ pointerEvents: "none" }}><FontAwesomeIcon icon={faUserCircle} /></span></div>
                            <div className="userTabCarry user">
                                <div className="pointerTab user" style={{ marginTop: "-13px", marginLeft: "162px", zIndex: 2 }}><div className="pointerTabBorder"></div></div>
                                <div className="userTab user" style={{ pointerEvents: "all" }}>
                                    <div className="userProfile user">{user === undefined ? "" : user.userProfile}</div>
                                    <div className="userName user">{user === undefined ? "" : user.name}</div>
                                    <div className="userBirthday user">Birthday : {user === undefined ? "" : user.birthday}</div>
                                    <div className="userCountry user">Country : {user === undefined ? "" : user.country}</div>
                                    <div className="userAddress user">Address : {user === undefined ? "" : user.address}</div>
                                    <div className="userEmail user">Email : {user === undefined ? "" : user.email}</div>
                                    <div className="userItemsNum user">Total items : {user === undefined ? "" : user.itemsList.length}</div>
                                    <button className="bt btn btn-primary logOut user" onClick={logOut}>Log out</button>
                                </div>
                            </div>
                        </li>
                        <li className="nav-item header-cart cart">
                            <div className="nav-link header-cart-icon cart" onClick={() => {
                                document.querySelector(".cartTabCarry").classList.toggle("tabShow")
                            }}><span style={{ pointerEvents: "none" }}><FontAwesomeIcon icon={faShoppingCart} /></span></div>
                            <div className="cartTabCarry cart">
                                <div className="pointerTab cart" style={{ marginTop: "-13px", marginLeft: "235px", zIndex: 2 }}><div className="pointerTabBorder" style={{ borderColor: "transparent transparent rgb(102, 133, 233) transparent" }}></div></div>
                                <div className="cartTab cart">
                                    <CartItems user={user} usersInfo={props.usersInfo} />
                                </div>
                            </div>
                        </li>
                        <li className="nav-item header-preferences">
                            <div className="nav-link"><FontAwesomeIcon icon={faEllipsisV} /></div>
                        </li>
                    </ul>
                </nav>
            </div>
            <SignUp usersInfo={props.usersInfo} />
            <SignIn update={store.getState().updateHeader} usersInfo={props.usersInfo} />
        </>)
}