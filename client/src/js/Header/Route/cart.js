import { store } from './../../../App.js'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { useState } from 'react'
import axios from 'axios'

var itemsList = [];
export function renewList() { itemsList = [] }
function Items(props) {
    var list = [];
    for (var j = 0; j < itemsList.length; j++)
        list.push(<div className="cartItem cart" key={j}>
            <img className="reImg cart" src={itemsList[j] === undefined ? "" : "Items/" + itemsList[j].id + itemsList[j].name + itemsList[j].type + ".png"} alt="" />
            <div className="cartItemInfo cart">
                <div className="cartItemName itemInfo cart"><div className="setTextRange cart">Name : {itemsList[j] === undefined ? "" : itemsList[j].name}</div></div>
                <div className="cartItemCost itemInfo cart"><div className="setTextRange cart">Cost : {itemsList[j] === undefined ? "" : itemsList[j].cost + "$"}</div></div>
            </div>
            <div className="cartDelete cart" id={j} onClick={(e) => {
                const num = store.getState().currentUser.userId
                const item = e.target.getAttribute("id")
                var usersInfo = props.usersInfo
                for (var i = 0; i < usersInfo.length; i++)
                    if (usersInfo[i].userId === num)
                        usersInfo[i].itemsList.splice(item, 1);
                axios.post("/users/post", usersInfo)
                store.getState().updateHeader()
            }}><FontAwesomeIcon icon={faTimes} className="cart" style={{ pointerEvents: "none" }} /></div>
        </div>
        )
    if (document.querySelector(".setTextRange") !== null) {
        const text = document.querySelectorAll(".setTextRange")
        const textRange = [];
        for (var i = 0; i < text.length; i++) { textRange.push(text[i].clientWidth) }
        textRange.forEach((textItem, i) => {
            if (textItem > 135) {
                text[i].addEventListener("mouseover", () => {
                    text[i].style.transition = `cubic-bezier(0,0,0,0) ${(textItem - 135) / 75}s`
                    text[i].style.transform = `translateX(-${textItem - 135}px)`
                })
                text[i].addEventListener("mouseout", () => {
                    text[i].style.transition = ``
                    text[i].style.transform = ``
                })
            }
        })
    }
    return (
        <div className="cartItemsHandle cart">
            {list}
        </div>
    )
}
export function CartItems(props) {
    const [state, setState] = useState(0)
    function update() {
        setState(state + 1);
    }
    store.dispatch({
        type: "updateItems", func: function (states) {
            states.updateItems = update;
            return states
        }
    })

    if (props.user !== undefined)
        itemsList = props.user.itemsList
    return (
        <>
            <div className="cartTotalItems cart">In cart : {itemsList.length}</div>
            <Items usersInfo={props.usersInfo} />
        </>
    )
}