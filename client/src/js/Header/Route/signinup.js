import { store } from './../../../App.js'
import axios from 'axios'

export function closeSignUp() {
    const signUp = document.querySelector(".signUpCarry");
    const disable = document.querySelector(".disableAll");
    signUp.style.animation = "hidden 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
    disable.style.animation = "hidden 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
    setTimeout(() => {
        signUp.style.display = "none";
        disable.style.display = "none";
    }, 500);
}
export function closeSignIn() {
    const signIn = document.querySelector(".signInCarry");
    const disable = document.querySelector(".disableAll");
    signIn.style.animation = "hidden 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
    disable.style.animation = "hidden 0.5s cubic-bezier(.62,.25,.4,1.21) forwards"
    setTimeout(() => {
        signIn.style.display = "none";
        disable.style.display = "none";
    }, 500);
}

export function SignUp(props) {
    function Submit() {
        document.querySelector(".errorSignUp").innerHTML = ``;
        const email = document.getElementById("mailUp").value
        const password = document.getElementById("passUp").value
        const repass = document.getElementById("repass").value
        const day = document.getElementById("day").value
        const month = document.getElementById("month").value
        const year = document.getElementById("year").value
        const country = document.getElementById("country").value
        const name = document.getElementById("name").value
        const address = document.getElementById("address").value
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const checkMail = re.test(String(email).toLowerCase());
        if (email !== "" && password !== "" && name !== "" && repass !== "" && day !== "" && month !== "" && year !== "" && country !== "" && address !== "")
            if (password === repass && checkMail) {
                var usersInfo = props.usersInfo
                // console.log(props.usersInfo);
                var usersId = usersInfo[usersInfo.length - 1].userId + 1
                usersInfo.push({
                    userId: usersId,
                    email: email,
                    password: password,
                    name: name,
                    birthday: day + " - " + month + " - " + year,
                    country: country,
                    address: address,
                    userProfile: "none",
                    itemsList: []
                })
                axios.post("/users/post", usersInfo)

                //Post here


                closeSignUp()
                document.querySelector(".errorSignUp").innerHTML = ``;
            }
        const sendMessage = [email !== "", password !== "", repass !== "", name !== "", day !== "", month !== "", year !== "", country !== "", address !== ""]
        const messageChosen = ["email", "password", "repass", "name", "day", "month", "year", "country", "address"]
        var message = ""
        for (var i = 0; i < sendMessage.length; i++)
            if (!sendMessage[i]) {
                message = messageChosen[i]; break;
            }
        if (message !== "")
            document.querySelector(".errorSignUp").innerHTML = `&#9888; Please enter ${message} !`;
        if (password !== repass)
            document.querySelector(".errorSignUp").innerHTML = `&#9888; Retype Password not match !`;
        if (!checkMail)
            document.querySelector(".errorSignUp").innerHTML = `&#9888; Please enter valid email !`;
    }
    return (
        <>
            <div className="signUpCarry">
                <div className="signUp">
                    <div className="signUpForm">
                        <div className="row mb-3 form-item">
                            <label htmlFor="mailUp" className="col-sm-2 col-form-label">Email</label>
                            <div className="col-sm-8 inputSign">
                                <input type="email" className="form-control" id="mailUp" />
                            </div>
                        </div>
                        <div className="row mb-3 form-item">
                            <label htmlFor="passUp" className="col-sm-2 col-form-label">Password</label>
                            <div className="col-sm-8 inputSign">
                                <input type="password" className="form-control" id="passUp" />
                            </div>
                        </div>
                        <div className="row mb-3 form-item">
                            <label htmlFor="repass" className="col-sm-2 col-form-label">Retype Password</label>
                            <div className="col-sm-8 inputSign">
                                <input type="password" className="form-control" id="repass" />
                            </div>
                        </div>
                        <div className="row mb-3 form-item">
                            <label htmlFor="name" className="col-sm-2 col-form-label">Name</label>
                            <div className="col-sm-8 inputSign">
                                <input type="text" className="form-control" id="name" />
                            </div>
                        </div>
                        <div className="row mb-3 form-item">
                            <label htmlFor="date" className="col-sm-2 col-form-label">Birthday</label>
                            <div className="col-sm-3 " style={{ marginLeft: "35px" }}>
                                <input type="number" min="1" max="31" className="form-control" id="day" />
                            </div>
                            <div className="col-sm-3 " style={{ marginLeft: "-7px" }}>
                                <input type="number" min="1" max="12" className="form-control" id="month" />
                            </div>
                            <div className="col-sm-3 inputSign">
                                <input type="number" min="1900" max="2021" className="form-control" id="year" />
                            </div>
                        </div>
                        <div className="row mb-3 form-item">
                            <label htmlFor="country" className="col-sm-2 col-form-label">Country</label>
                            <div className="col-sm-8 inputSign">
                                <input type="text" className="form-control" id="country" />
                            </div>
                        </div>
                        <div className="row mb-3 form-item">
                            <label htmlFor="address" className="col-sm-2 col-form-label">Address</label>
                            <div className="col-sm-8 inputSign">
                                <input type="text" className="form-control" id="address" />
                            </div>
                        </div>
                        <div className="errorSignUp"></div>
                        <button onClick={Submit} className="btn btn-primary form-item">Sign up</button>
                    </div>
                </div>
            </div>
        </>
    )
}
export function SignIn(props) {
    function Submit() {
        document.querySelector(".errorSignIn").innerHTML = ``;
        const email = document.getElementById("mailIn").value
        const password = document.getElementById("passIn").value
        const sendMessage = [email !== "", password !== ""]
        const messageChosen = ["email", "password"]
        var message = ""
        for (var i = 0; i < sendMessage.length; i++)
            if (!sendMessage[i]) {
                message = messageChosen[i]; break;
            }
        document.querySelector(".errorSignIn").innerHTML = `&#9888; Please enter ${message} !`;
        var user = {};
        for (var j = 0; j < props.usersInfo.length; j++) {
            if (props.usersInfo[j].email === email) {
                user = props.usersInfo[j]
            }

        }
        if (user.email === undefined) {
            if (email !== "")
                document.querySelector(".errorSignIn").innerHTML = `&#9888; Account not available !`;
        } else {
            if (password !== user.password) {
                if (password !== "")
                    document.querySelector(".errorSignIn").innerHTML = `&#9888; Password Incorrect !`;
            }
            else {
                closeSignIn();
                document.querySelector(".errorSignIn").innerHTML = ``
                store.dispatch({
                    type: "user", func: function (state) {
                        state.currentUser = user
                        return state
                    }
                })
                document.getElementById("currentUser").classList.remove("disabled")
                document.getElementById("signIn").classList.add("disabled")
                setTimeout(() => {
                    props.update();
                }, 100);
            }
        }
    }
    return (
        <>
            <div className="signInCarry">
                <div className="signIn">
                    <div className="signInForm">
                        <div className="row mb-3 form-item">
                            <label htmlFor="mailIn" className="col-sm-2 col-form-label">Email</label>
                            <div className="col-sm-8 inputSign">
                                <input type="email" className="form-control" id="mailIn" />
                            </div>
                        </div>
                        <div className="row mb-3 form-item">
                            <label htmlFor="passIn" className="col-sm-2 col-form-label">Password</label>
                            <div className="col-sm-8 inputSign">
                                <input type="password" className="form-control" id="passIn" />
                            </div>
                        </div>
                        <div className="errorSignIn"></div>
                        <button onClick={Submit} className="btn btn-primary form-item">Sign in</button>
                    </div>
                </div>
            </div>
        </>
    )
}