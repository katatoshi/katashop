import "./../../css/Dashboard/dashboard.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faBalanceScaleLeft, faBolt, faShoePrints, faBriefcase, faBezierCurve, faGlobeEurope, faTshirt } from '@fortawesome/free-solid-svg-icons'
import { Link } from 'react-router-dom'
export default function Dashboard() {
    function closeBars() {
        const bars = document.querySelector(".boardCarry");
        if (bars != null) {
            bars.style.animation = `navbarsreve cubic-bezier(.62,.25,.4,1.21) 0.5s forwards`
        }
        document.querySelector(".header-dashboard").style.animation = `navbarsresizereve cubic-bezier(.62,.25,.4,1.21) 0.5s forwards`
        // document.querySelector(".header__nav-route").style.animation = `routeresizebybarreve cubic-bezier(.62,.25,.4,1.21) 0.5s forwards`
        // document.querySelector(".header__nav-route").style.position = `absolute`;
        // document.querySelector(".header__nav-route").style.transform = ``;
        document.querySelector(".AllItemCarry").style.width = `100vw`;
    }
    document.addEventListener("click", (e) => {
        if (e.target != null)
            if (e.target.classList.contains("body")
                && document.querySelector(".boardCarry").getBoundingClientRect().x > -105
            ) closeBars();
    })
    return (
        <>
            <div className="boardCarry" >
                <div className="boardBars opachover" onClick={closeBars}><FontAwesomeIcon icon={faBars} /></div>
                <nav className="nav navbar-light d-flex align-items-start boardItemsCarry">
                    <ul className="navbar-nav flex-column nav-pills me-3" >
                        <li className="nav-item">
                            <Link to="/all" className="nav-link boardItem"><FontAwesomeIcon icon={faGlobeEurope} />&nbsp; All Items</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/sale" className="nav-link boardItem"><FontAwesomeIcon icon={faBalanceScaleLeft} />&nbsp; Sale</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/newcollection" className="nav-link boardItem"><FontAwesomeIcon icon={faBolt} />&nbsp; New Collection</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/clothes" className="nav-link boardItem"><FontAwesomeIcon icon={faTshirt} />&nbsp; Clothes</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/shoes" className="nav-link boardItem"><FontAwesomeIcon icon={faShoePrints} />&nbsp; Shoes</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/bags" className="nav-link boardItem"><FontAwesomeIcon icon={faBriefcase} />&nbsp; Bags Carry</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/accessories" className="nav-link boardItem"><FontAwesomeIcon icon={faBezierCurve} />&nbsp; Accessories</Link>
                        </li>
                    </ul>
                </nav>
            </div>
        </>)
}