import React from 'react';
import './keyframes.css';
import './App.css';
import './Bootrap5/css/bootstrap.css'
import Header from './js/Header/header.js';
import Dashboard from './js/Dashboard/dashboard.js';
import Body from './js/Body/body.js';
import Footer from './js/Footer/footer.js';
import { BrowserRouter as Router } from 'react-router-dom'
import { createStore } from 'redux';
import { closeSignIn, closeSignUp } from './js/Header/Route/signinup.js'
import axios from 'axios';

//Use key "func" when create a callback for redux
//state.currentUser
; (function () {
  document.addEventListener("scroll", () => {
    const headerbar = document.querySelector(".header__bar")
    if (headerbar != null)
      if (headerbar.getBoundingClientRect().y < -90) {
        headerbar.style.marginTop = "-90px"
        headerbar.style.position = "fixed"
        setTimeout(() => {
          headerbar.style.transition = "1s cubic-bezier(.62,.25,.4,1.21)"
          headerbar.style.marginTop = "0"
        }, 0);
      } else {
        if (headerbar.style.position === "fixed"
          && document.getElementById("root").getBoundingClientRect().y > -10
        ) {
          headerbar.style.transition = ""
          headerbar.style.position = "absolute"
        }
      }
  })
})()
const store = createStore((state = {}, action) => {
  if ("func" in action)
    return action.func(state)
})

const accessories = () => {
  return axios.get('/api/accessories')
    .then((res) => {
      return res.data
    })
}
const clothes = () => {
  return axios.get('/api/clothes')
    .then((res) => {
      return res.data
    })
}
const bags = () => {
  return axios.get('/api/bags')
    .then((res) => {
      return res.data
    })
}
const users = () => {
  return axios.get('/users/get')
    .then((res) => {
      return res.data
    })
}
const shoes = () => {
  return axios.get('/api/shoes')
    .then((res) => {
      return res.data
    })
}

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      accessories: null,
      bags: null,
      clothes: null,
      shoes: null,
      usersInfo: null
    }
  }

  componentWillMount() {
    if (this.state.accessories === null) {
      accessories().then((data) => {
        this.setState({
          accessories: data
        })
      })
    }
    if (this.state.bags === null) {
      bags().then((data) => {
        this.setState({
          bags: data
        })
      })
    }
    if (this.state.shoes === null) {
      shoes().then((data) => {
        this.setState({
          shoes: data
        })
      })
    }
    if (this.state.clothes === null) {
      clothes().then((data) => {
        this.setState({
          clothes: data
        })
      })
    }
    if (this.state.usersInfo === null) {
      users().then((data) => {
        this.setState({
          usersInfo: data
        })
      })
    }
  }

  render() {
    return (
      <div className="App">
        <Router>
          <div className="setRangeText"></div>
          <div className="disableAll" onClick={() => {
            if (document.querySelector(".signUpCarry").style.display !== "none")
              closeSignUp()
            if (document.querySelector(".signInCarry").style.display !== "none")
              closeSignIn()
          }}></div>
          <Header usersInfo={this.state.usersInfo} />
          <Dashboard />
          <Body usersInfo={this.state.usersInfo} />
          <Footer />
        </Router>
      </div>
    )
  }
}

export default App;
export { store }